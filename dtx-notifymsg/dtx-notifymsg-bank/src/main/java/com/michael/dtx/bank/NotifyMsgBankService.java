package com.michael.dtx.bank;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableHystrix
@EnableFeignClients(basePackages = {"com.michael.dtx.bank.spring"})
public class NotifyMsgBankService {

	public static void main(String[] args) {
		SpringApplication.run(NotifyMsgBankService.class, args);
	}

}
