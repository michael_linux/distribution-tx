package com.michael.dtx.bank.controller;

import com.michael.dtx.bank.service.AccountInfoService;
import com.michael.dtx.bank.entity.AccountPay;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Administrator
 * @version 1.0
 **/
@RestController
@Slf4j
public class AccountInfoController {
    /**
     * 充值系统
     * http://127.0.0.1:56082/pay/paydo?accountNo=1&payAmount=10
     * http://127.0.0.1:56082/pay/paydo?accountNo=1&payAmount=2
     * 账户系统 充值回查
     * http://127.0.0.1:56081/bank/payresult/{txNo}
     */
    @Autowired
    private AccountInfoService accountInfoService;

    //主动查询充值结果
    @GetMapping(value = "/payresult/{txNo}")
    public AccountPay result(@PathVariable("txNo") String txNo){
        AccountPay accountPay = accountInfoService.queryPayResult(txNo);
        return accountPay;
    }
}
