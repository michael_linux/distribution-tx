package com.michael.dtx.seatademo.bank1.controller;

import com.michael.dtx.seatademo.bank1.service.AccountInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Administrator
 * @version 1.0
 **/
@RestController
public class Bank1Controller {

    @Autowired
    AccountInfoService accountInfoService;

    // http://127.0.0.1:56081/bank1/transfer?amount=300
    // http://127.0.0.1:56081/bank1/transfer?amount=2
    // http://127.0.0.1:56081/bank1/transfer?amount=3
    //张三转账
    @GetMapping("/transfer")
    public String transfer(Double amount){
        accountInfoService.updateAccountBalance("1",amount);
        return "bank1 -- "+amount;
    }
}
