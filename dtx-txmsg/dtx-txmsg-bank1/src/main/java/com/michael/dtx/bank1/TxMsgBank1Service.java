package com.michael.dtx.bank1;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TxMsgBank1Service {

	public static void main(String[] args) {
		SpringApplication.run(TxMsgBank1Service.class, args);
	}

}
/**
 * http://127.0.0.1:8081/bank1/transfer?accountNo=1&amount=11
 */