package com.michael.dtx.bank1.controller;

import com.michael.dtx.bank1.model.AccountChangeEvent;
import com.michael.dtx.bank1.service.AccountInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author Administrator
 * @version 1.0
 **/
@RestController
@Slf4j
public class AccountInfoController {
    @Autowired
    private AccountInfoService accountInfoService;

    /**
     * http://127.0.0.1:8081/bank1/transfer?accountNo=1&amount=100
     *
     * http://127.0.0.1:8081/bank1/transfer?accountNo=1&amount=3
     *
     * http://127.0.0.1:8081/bank1/transfer?accountNo=1&amount=4
     * @param accountNo
     * @param amount
     * @return
     */
    @GetMapping(value = "/transfer")
    public String transfer(@RequestParam("accountNo")String accountNo, @RequestParam("amount") Double amount){
        //创建一个事务id，作为消息内容发到mq
        String tx_no = UUID.randomUUID().toString();
        AccountChangeEvent accountChangeEvent = new AccountChangeEvent(accountNo,amount,tx_no);
        //发送消息
        accountInfoService.sendUpdateAccountBalance(accountChangeEvent);
        return "转账成功";
    }
}
