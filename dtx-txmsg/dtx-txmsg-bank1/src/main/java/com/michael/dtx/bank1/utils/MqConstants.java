package com.michael.dtx.bank1.utils;

public class MqConstants {

    public static final String PRODUCER_GROUP_TXMSG_BANK1 = "producer_group_txmsg_bank1";
    public static final String CONSUMER_GROUP_TXMSG_BANK2 = "consumer_group_txmsg_bank2";
    public static final String TOPIC_TXMSG = "topic_txmsg";

}
