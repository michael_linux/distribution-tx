DROP DATABASE IF EXISTS `bank1_pay`;
CREATE DATABASE `bank1_pay` CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';
USE `bank1_pay`;

DROP TABLE IF EXISTS `account_pay`;
CREATE TABLE `account_pay` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `account_no` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '账号',
  `pay_amount` double DEFAULT NULL COMMENT '充值余额',
  `result` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '充值结果:success，fail',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;

insert  into `account_pay`(`id`,`account_no`,`pay_amount`,`result`)
values ('5678ef0a-1ff0-4cfd-97ac-640d749d596f','1',2,'success'),
       ('7d7d469c-f100-4066-b927-014c0c3aa010','1',2,'success'),
       ('947fafad-c19c-46bc-b0f0-43703a124fd4','1',2,'success');
